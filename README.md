# Drupal settings development split

For this split to have any effect you need to have an active
config split entity in the configuration like this:
```
uuid: c19abad3-e6cc-4eea-a5fa-2e372eedde8a
langcode: en
status: false
dependencies: {  }
id: development
label: Development
description: ''
weight: 0
storage: folder
folder: ../vendor/upstreamable/drupal-settings-dev-split/split
module:
  config: 0
  devel: 0
  field_ui: 0
  views_ui: 0
theme: {  }
complete_list: {  }
partial_list: {  }
```
